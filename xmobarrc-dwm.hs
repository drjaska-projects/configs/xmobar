Config { font = "-*-Fixed-Bold-R-Normal-*-13-*-*-*-*-*-*-*"
       , additionalFonts = [ "xft:FontAwesome:pixelsize=13" ]
       , bgColor = "#000000"
    -- , fgColor = "#28AE1E" -- green
       , fgColor = "#FFA500" -- orange
       , position = TopW L 100
       , sepChar = "%"
       , alignSep = "}{"
       , allDesktops = True
       , persistent = True
       , template = "%multicpu% | %cpu% %coretemp%    %dynnetwork%    %memory%    <fc=#ee9a00>%date%</fc>    %EFJY%"
       , commands =[
         Run StdinReader

           -- all(4) cpu cores activity monitor
           , Run MultiCpu       [ "--template" , "CPU: <total0>% * <total1>% * <total2>% * <total3>%"
                                , "--Low"      , "50"         -- units: %
                                , "--High"     , "85"         -- units: %
                             -- , "--low"      , "#013220"
                             -- , "--normal"   , "#FF8C00"
                             -- , "--high"     , "#FF0000"
                                , "--low"      , "#FFA500"
                                , "--normal"   , "#FF8C00"
                                , "--high"     , "#FF0000"
                                ] 10 --number of tenths of second between updates, 1s

           -- cpu activity monitor
           , Run Cpu            [ "--template" , "CPU: <total>%"
                                , "--Low"      , "50"         -- units: %
                                , "--High"     , "85"         -- units: %
                             -- , "--low"      , "#013220"
                             -- , "--normal"   , "#FF8C00"
                             -- , "--high"     , "#FF8C00"
                                , "--low"      , "#FFA500"
                                , "--normal"   , "#FF8C00"
                                , "--high"     , "#FF0000"
                                ] 10 --number of tenths of second between updates, 1s

           -- cpu core temperature monitor
           , Run CoreTemp       [ "--template" , "Temp: <core2>°C"
                                , "--Low"      , "40"         -- units: °C
                                , "--High"     , "60"         -- units: °C
                             -- , "--low"      , "#013220"
                             -- , "--normal"   , "#FF8C00"
                             -- , "--high"     , "#FF0000"
                                , "--low"      , "#FFA500"
                                , "--normal"   , "#FF8C00"
                                , "--high"     , "#FF0000"
                                ] 300 --number of tenths of second between updates, 30s

           -- network activity monitor (dynamic interface resolution)
           , Run DynNetwork     [ "--template" , "<rx>kB/s|<tx>kB/s"
                                , "--Low"      , "3000"       -- units: B/s
                                , "--High"     , "50000"      -- units: B/s
                             -- , "--low"      , "#013220"
                             -- , "--normal"   , "#FF8C00"
                             -- , "--high"     , "#FF0000"
                                , "--low"      , "#FFA500"
                                , "--normal"   , "#FF8C00"
                                , "--high"     , "#FF0000"
                                ] 10 --number of tenths of second between updates, 1s

           -- ram activity monitor
           , Run Memory         [ "--template" , "Mem: <usedratio>%"
                                , "--Low"      , "50"         -- units: %
                                , "--High"     , "85"         -- units: %
                             -- , "--low"      , "#013220"
                             -- , "--normal"   , "#FF8C00"
                             -- , "--high"     , "#FF0000"
                                , "--low"      , "#FFA500"
                                , "--normal"   , "#FF8C00"
                                , "--high"     , "#FF0000"
                                ] 10 --number of tenths of second between updates, 1s

           -- date: Day Month yyyy-mm-dd hh:mm:ss
           , Run Date           "%a %b %F %H:%M:%S" "date" 10

           -- weather: Temperature from EFJY in Central Finland and hopefully
           -- sky condition too one day but if not then it displays nothing
           , Run Weather "EFJY" [ "--template" , "Weather: <skyCondition> <tempC>°C"
                                , "--Low"      , "5"          -- units: °C
                                , "--High"     , "20"         -- units: °C
                             -- , "--low"      , "#ADD8E6"
                             -- , "--normal"   , "green"
                             -- , "--high"     , "yellow"
                                , "--low"      , "#ADD8E6"
                                , "--normal"   , "#FFA500"
                                , "--high"     , "#FF0000"
                                ] 3600 --number of tenths of second between updates, 6min
       ]
       }
