#!/bin/bash -eu

# search all .xmobarrc files and update them to match xmobarrc.hs
# to create new .xmobarrc file for let's say monitor 5 do the below line
# touch "$HOME"/.config/xmobar/.xmobarrc-5

for i in $(ls -a "$HOME"/.config/xmobar/ | grep \.xmobarrc-)
do
	cp "$HOME"/.config/xmobar/xmobarrc.hs "$HOME"/.config/xmobar/$i
	echo "Updated $i"
done

echo "Updated configs"

